#!/usr/bin/python

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders
import os
import sys
import serial
import time
import datetime
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.dates as mdates
import matplotlib.cbook as cbook
import dateutil.parser as dparser
from pylab import *


#CHANGEABLES :)
TimeBetweenUpdatesInMinutes = 1; #set the freuncy of update emails
Arm_Security = True	#enable to send emails on alarm state.


csv_success = False

gmail_user = "#####" #your gmail account
gmail_pwd = "#####"	 #your gmail password
	
'''base email function. Give address, text and attachment.'''
def mail(to, subject, text, attach='logo.jpg'):
	#create new multimedia mail obj
	msg = MIMEMultipart()

	msg['From'] = gmail_user
	msg['To'] = to
	msg['Subject'] = subject

	msg.attach(MIMEText(text))
	
	#setup attachment encoder.
	part = MIMEBase('application', 'octet-stream')
	part.set_payload(open(attach, 'rb').read())
	Encoders.encode_base64(part)
	part.add_header('Content-Disposition','attachment; filename="%s"' % os.path.basename(attach))
	msg.attach(part)

	#create SMTP mail server and send email!
	mailServer = smtplib.SMTP("smtp.gmail.com", 587)	#GMAIL port.
	mailServer.ehlo()
	mailServer.starttls()
	mailServer.ehlo()
	mailServer.login(gmail_user, gmail_pwd)
	mailServer.sendmail(gmail_user, to, msg.as_string())
	mailServer.close()	
	
'''writeToCsv is passed a list of data and saves it to a csv file.'''
def writeToCsv(datalist):
	#column row titles (used again when parsing to matlibplot
	header = ["date", "time","Temp", "Humidity", "Pressure", "Motion","Reed","Trip","Alarm"]
	
	fileName = str(time.strftime("%m_%d_%y_")+ "log.csv")
	
	#if no file exists, create one and add header row 0, else open in append mode.
	if os.path.exists(fileName):
		f = open(fileName, "a")
	else:
		f = open(fileName, "a+")
		for element in header:
			f.write(element + ",")
		f.write("\n")

	#add data to csv file. comma deliminated.
	for element in datalist:
		if type(element)==str:
			f.write(element + ",")
		if type(element) == list:
			for i in element:
				f.write(i + ",")
	f.write("\n")
	f.close()
	csv_success = True
  
#function grabs current date and time and returns in 2-element list. [Date,Time]
def getDateTime():
	timeNow = time.strftime("%H:%M:%S")
	dateToday = time.strftime("%m/%d/%y")
	return [dateToday, timeNow]
	
	
'''
	Data is sent to the PC in VAL;VAL;VAL;VAL format
	The data is converted by the arduino and then forwarded
	Order of data is
	[0] - Temperature - float
	[1] - Humiditiy - float
	[2] - Pressure - float
	[3] - Motion Sensor - Boolean
	[4] - Reed Switch - Boolean
	[5] - Sonar - Boolean (arduino reads and if < threshold, something in the way!)
	[6] - Alarm Armed? - Boolean	
'''

def plotData():
	fileName = str(time.strftime("%m_%d_%y_")+ "log.csv")
	
	#read from excel file and save to numpy array.
	data = np.genfromtxt(
		fileName,
		delimiter=',',
		usecols=(1,2,3,4,7),
		skip_header=1,
		names=['time','temp','humid','pressure','alarm'],	
		dtype ='object',
		converters = {'time':dparser.parse})


	#parse each column of data.
	timeSeries = data['time']
	temp = data['temp']	
	humidity = data['humid']	
	pressure = data['pressure']	
	alarm =  data['alarm']	
	
	plt.figure(1)
	
	#plot the temperature
	ax1 = subplot(311)
	plot_date(timeSeries,temp,xdate=True,ydate=False,linestyle='-',marker=None)
	ylabel('Temperature (C)')
	title('Enviroment Update!')
	gca().get_yaxis().get_major_formatter().set_useOffset(False)	
	
	#plot the humidity
	ax2 = subplot(312,sharex=ax1)
	plot_date(timeSeries,humidity,xdate=True,ydate=False,linestyle='-',marker=None)
	ylabel('Humiditiy(%RH)')
	gca().get_yaxis().get_major_formatter().set_useOffset(False)
	
	#plot the pressure
	ax3 = subplot(313,sharex=ax1)
	plot_date(timeSeries,pressure,xdate=True,ydate=False,linestyle='-',marker=None)	
	ylabel('Pressure(kPa)')
	
	#Format only time data, ignore date. 
	xfmt = mdates.DateFormatter('%H:%M:%S')
	ax3.xaxis.set_major_formatter(xfmt)
	ax3.tick_params(axis='x',labelsize=8)
	
	gca().get_yaxis().get_major_formatter().set_useOffset(False)
	matplotlib.dates.AutoDateLocator()
	gcf().autofmt_xdate()
	
	#hide ticks on upper plots. Share lower X-Axis
	setp( ax1.get_xticklabels(), visible=False)	
	setp( ax2.get_xticklabels(), visible=False)	

	pp = PdfPages('EnviromentUpdate.pdf')
	plt.savefig(pp,format='pdf')
	pp.close()	
	

def mainLoop():
	refTime = time.time()
	
	data = []
	
	print "Attempting Connection with com port..."
	#on each connect, change the comm port to match arduino
	try:
		ser = serial.Serial("COM7",9600)
		print "Serial Initialized"
		loop = 1
	except:
		print "Failed to connect to comm port, is it in use? Correct Port?"
		loop = 0
		
		
	needUpdate = 0
	while loop:
		if ser.inWaiting():
			datetimeData = getDateTime()
			for i in datetimeData:
				data.append(i)
				
			sensorData = ser.readline().strip('\n\r')
			
			#we received valid data. Parse it!
			if (sensorData.find('{') > -1 and sensorData.find('}') > -1):
				sensorData = sensorData.strip('{').strip('}')
				sensorData = sensorData.split(';')			
				
				print sensorData
				
				data.append(sensorData)		
				
				#for event based email.
				if Arm_Security == True:
					if (needUpdate == 0):
						#motion sensor triggered						
						if sensorData[6] == '1':
							print 'Intruder Detected in House!'
							mail("thomasryan@rogers.com",
							"URGENT UPDATE",
							"A presence is Detected in your home! Alarm is active. Notification when deactivated.")		
							needUpdate = 1
							
					#wait until alarm is disabled. Once it is, send an update.	
					else:
						if sensorData[6] == '0':
							print 'Alarm Deactivated!'
							mail("thomasryan@rogers.com",
							"ALARM DEACTIVATED",
							"Alarm has been successfully deactivated. System is secure")	
							needUpdate = 0
							
				#write the retrieved data to CSV file
				try:
					writeToCsv(data)						
				except:
					print 'Failed to write to CSV, ensure file not in use by another program'
					
				
				try:
					#send update email every TimeBetweenUpdatesInMinutes
					if (time.time() - refTime) > TimeBetweenUpdatesInMinutes*60:
						refTime = time.time()
						plotData()
						print 'Sending update email...'
						
						try:	
							mail("thomasryan@rogers.com",
							"Data Update",
							"TG Security Systems has a data update for you!",
							'EnviromentUpdate.pdf')						
							print 'Email send succesful'
						except:
							print 'Failed to send email'
						
				except:
					print 'Failed to plot data, check csv files'
				
				
			data = []
			
mainLoop()

	
	
	
	
	