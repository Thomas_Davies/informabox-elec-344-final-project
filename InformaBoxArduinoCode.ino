//CHANGELIST: added security unlock to main code, also cleaned up
//variable names + global/local variables. Unlock is not working atm
//Interrupt routine fires tons of times when you press, making all the 
//leds light up turn off... 
//


#include <Wire.h>
#include "HTU21D.h" //humidity temp library 
#include "MPL3115A2.h"//pressure library

//////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~PIN DEFINES~~~~~~~~~~~~~~~~~~~~~~//
//////////////////////////////////////////////////////////
//sensor pins
int pirPin = 9;        //NEED CHANGE ON CICUIT
int reedPin = 10;      //NEED CHANGE ON CIRCUIT
int sonarPin = A3;
//security led pins
int SecurityLedOne = 7;
int SecurityLedTwo = 6;
int SecurityLedThree = 5;
int SecurityLedFour = 4;
//security buttons
int SecuritySelectPin = 2;
int SecurityMoveOnPin = 3;
//security siren pin
int SecuritySirenPin = 8;
//

//////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~GLOBAL VARIABLES~~~~~~~~~~~~~~~~~~~//
//////////////////////////////////////////////////////////

int threshold = 10;
boolean alarmActive =  false;
boolean reset =true;

int LedStates[4] = {0,0,0,0};
int UnlockCode[4] = {1,0,1,0};

unsigned long DataLastMillis = 0;
unsigned long timeSinceDeactivated =millis();
boolean deactivatedRecently = false;

unsigned long previousMillis = 0;
int ledState = LOW;
//int sirenState = LOW;
volatile int currLed = 0;                      //current LED in code entry
boolean firstRun = true;              //attachInrupts if true, otherwise skip

//Create Humidity sensor object
HTU21D environmnent;


//Create pressure sensor object
MPL3115A2 pressure;

void setup()
{
  //noInterrupts();
  Serial.begin(9600);
  environmnent.begin();
  
  pressure.begin();
  pressure.setModeBarometer();// puts pressure sensor in Pascal measurement mode
  
  pinMode(pirPin, INPUT);  
  pinMode(reedPin, INPUT);
  pinMode(sonarPin,INPUT);
  pinMode(SecurityLedOne,OUTPUT);
  pinMode(SecurityLedTwo,OUTPUT);
  pinMode(SecurityLedThree,OUTPUT);
  pinMode(SecurityLedFour,OUTPUT);
  pinMode(SecuritySelectPin,INPUT);
  pinMode(SecurityMoveOnPin,INPUT);
}

void loop()
{  
  if (alarmActive)
  {
     if(!digitalRead(SecuritySelectPin)){
      comboSelect();
     }
     else if(!digitalRead(SecurityMoveOnPin)){
      comboSkip();
     }
      //set led states
      setLeds();
      ledBlinker(currLed);      
  }
  
  //Get values from sensors.
  float temperature = environmnent.readTemperature();
  float humidity = environmnent.readHumidity();  
  float pressureValue = (pressure.readPressure())/1000;   
  
  boolean isPIR = digitalRead(pirPin);
  boolean isReed =  false;      //digitalRead(reedPin);
  boolean isSonar = false;  
  
  float inches = analogRead(sonarPin)/2.0;
  if(inches < threshold)
    isSonar = true;
  
  if (!alarmActive){
    if (!deactivatedRecently){
        if((!isPIR||isReed|| isSonar)){
          alarmActive = true;
          reset = false;
        }
    }
    else if ((millis() - timeSinceDeactivated) > 2000)
    {
        deactivatedRecently = false;        
    }
  }

  
  
  unsigned long DataCurrentMillis = millis();
  
  if ((DataCurrentMillis - DataLastMillis) > 900)
  {
     DataLastMillis = DataCurrentMillis;
     Serial.print('{');  
     Serial.print(temperature);
     Serial.print(";");
     Serial.print(humidity);
     Serial.print(";");
     Serial.print(pressureValue);
     Serial.print(";");
     Serial.print(!isPIR);
     Serial.print(";");
     Serial.print(isReed);
     Serial.print(";");
     Serial.print(isSonar);
     Serial.print(";");
     Serial.print(alarmActive);
     Serial.print("}\n\r");
  }
   
}

void comboSkip()
{
  
    currLed ++; 
    codeCheckAndReset();
    delay(200);
  
}

void comboSelect()
{
     
      LedStates[currLed] = 1;
  
      codeCheckAndReset();
      delay(200);
    
}

void ledBlinker(int i)
{
      tone(SecuritySirenPin,1200,100);
       //blink current led.
      switch (i) {
         case 0:
            blinkLed(SecurityLedOne);
            break; 
         case 1:
            blinkLed(SecurityLedTwo);
            break;
         case 2:
           blinkLed(SecurityLedThree);
           break;
         case 3:
           blinkLed(SecurityLedFour);
           break;
         default:
           break;
       } 
}
void blinkLed(int ledPin)
{
   unsigned long currentMillis = millis();
   if (currentMillis - previousMillis >= 200)
   {
      previousMillis = currentMillis;
      if (ledState == LOW){
        ledState = HIGH;
      }
      else{
        ledState = LOW;
      }
      digitalWrite(ledPin,ledState);
   }
}

boolean codeCorrect()
{
   boolean correct = true;
   for (int i =0; i < 4; i++)
   {
     if (LedStates[i] != UnlockCode[i])
         correct = false;
   }
   if(correct)
     reset = true;
   return correct;
  
}

void codeCheckAndReset()
{
  if (codeCorrect())
  {
    timeSinceDeactivated = millis();
    deactivatedRecently = true;
    firstRun = true;
    //reset leds
    for (int i = 0; i < 4;i++)
      LedStates[i] = 0; 
    alarmActive = false;
    currLed = 0;
    tone(SecuritySirenPin,1000,500);  //long tone to indicate correct code.
    setLeds();                        //Reset leds!
  }

  else if (currLed == 3)
  {
    //reset leds
    for (int i = 0; i < 4;i++)
      LedStates[i] = 0; 
    Serial.println("Incorrect");
    currLed = 0;    
  }

}

void setLeds()
{  
   digitalWrite(SecurityLedOne,LedStates[0]);
   digitalWrite(SecurityLedTwo,LedStates[1]);
   digitalWrite(SecurityLedThree,LedStates[2]);
   digitalWrite(SecurityLedFour,LedStates[3]);
}


