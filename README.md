# README #

#Hardware setup:
     -arduino uno
     -HTU21D humidity and temperature sensor
     -MPL3115A2 pressure sensor
     -PIR motion sensor
     -Reed Switch
     -Ultrasonic Range finder

Python (x,y) was used for addition of numpy and matplotlib. These are the only non-standard dependencies.

### What is this repository for? ###

* showcasing elec 344 final project code.

### Contribution guidelines ###

* no contributions. This is final code.

### Who do I talk to? ###
For any questions contact developer:
* Thomas Davies - thomasryan@rogers.com